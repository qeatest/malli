package Cleartrippackagae;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class cleartripdriversetup {

	public static WebDriver driver;
	public WebDriver chromesetup()
	{
		System.setProperty("webdriver.chrome.driver","C:\\Users\\HP\\Desktop\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		ChromeOptions opt = new ChromeOptions();
		opt.addArguments("--disable-notifications");
		 driver = new ChromeDriver(opt);
		driver.get("https://www.cleartrip.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(300,TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(300,TimeUnit.SECONDS);
		return driver;
		
	}

}
