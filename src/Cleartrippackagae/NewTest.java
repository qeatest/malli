package Cleartrippackagae;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;

public class NewTest extends cleartripdriversetup {
	@BeforeMethod
	  public void beforeMethod() 
	{
		driver = chromesetup();
	  }
  @Test
  public void f() throws IOException, InterruptedException {
	  WebDriverWait wait =new WebDriverWait(driver,40);
	  WebElement el1 = driver.findElement(By.xpath("//input[@id='RoundTrip']"));
	  el1.click();
	  WebElement el2 = driver.findElement(By.xpath("//input[@id='FromTag']"));
	  el2.click();
	  el2.sendKeys("Hyderabad, IN - Rajiv Gandhi International (HYD)");
	  WebElement el3 = driver.findElement(By.xpath("//input[@id='ToTag']"));
	  el3.click();
	  el3.sendKeys("New Delhi, IN - Indira Gandhi Airport (DEL)");
	  WebElement el4 = driver.findElement(By.xpath("//input[@id='DepartDate']"));
	  el4.click();
	  WebElement el5 = driver.findElement(By.xpath("(//a[text()='12'])[1]"));
	  el5.click();
	  WebElement el14 = driver.findElement(By.xpath("//input[@id='ReturnDate']"));
	  el14.click();
	  WebElement el15 = driver.findElement(By.xpath("//a[text()='26']"));
	  el15.click();
	  
      Select el6 = new Select(driver.findElement(By.id("Adults")));
      el6.selectByVisibleText("2");
      Select el7 = new Select(driver.findElement(By.id("Childrens")));
      el7.selectByVisibleText("2");
      Select el8 = new Select(driver.findElement(By.id("Infants")));
      el8.selectByVisibleText("1");
      WebElement el9 = driver.findElement(By.xpath("//input[@id='SearchBtn']"));
	  el9.click();
	  Thread.sleep(13000);
	 File s2 =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
     FileUtils.copyFile(s2,new File("C:\\malli\\shiva12.png"));
	 
      
  }
  @AfterMethod
  public void afterMethod() 
  {
	  driver.close();
  }
 
}